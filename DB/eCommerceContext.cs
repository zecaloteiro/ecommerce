﻿using eCommerce.Models.Produtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.DB {
  public class eCommerceContext : DbContext {
    public DbSet<Categoria> DBCategoria;
    public DbSet<Produto> DBProduto;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
      optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;" +
        "Initial Catalog=master;" +
        "Integrated Security=True;" +
        "Connect Timeout=30;Encrypt=False;" +
        "TrustServerCertificate=False;" +
        "ApplicationIntent=ReadWrite;" +
        "MultiSubnetFailover=False");
    }
  }
}