﻿namespace eCommerce.Models.Produtos {
  public class Categoria {
    public int Id { get; }
    public string Descricao { get; set; }
    public Categoria CategoriaPai { get; set; }
    public bool Ativo { get; set; }
  }
}