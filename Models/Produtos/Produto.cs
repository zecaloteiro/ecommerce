﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Models.Produtos {
  public class Produto {
    public int Id { get; set; }
    public string Descricao { get; set; }
    public decimal Preco { get; set; }
    public Categoria Categoria { get; set; }
    public string Unidade { get; set; }
  }
}